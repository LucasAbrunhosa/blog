$(function(){
	$('.navbar-brand').html('Les voyages de Luc');
	$('#header').html('Découvrez mes voyages <small> En Europe </small>');

	var menu = ['Accueil','Qui suis-je ?','Voyages','Photos','Contacts'];
	for(var i=0, c=menu.length; i<c; i++) {
		$('#menu').append('<li class="nav-item"><a href="#" class="nav-link">' + menu[i] + '</li></a>');
	}

	var categories = ['Europe','Amérique Nord','Amérique Sud','Afrique','Asie'];
	for(var i=0, c=categories.length; i<c; i++) {
		$('#categories').append('<li class="col-lg-6"><a href="#">' + categories[i] + '</a></li>');
	}

	var article1 = {
		'image'  : "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Sarcelles_-_Hotel_de_ville_-_Panoramique_01.jpg/1280px-Sarcelles_-_Hotel_de_ville_-_Panoramique_01.jpg",
		'titre'  : "Sarcelles",
		'date'   : "29/11/18",
		'auteur' : "Abrunhosa Lucas",
		'lieu'   : "Mairie",
		'texte'  : "Hac ita persuasione reducti intra moenia bellatores obseratis undique portarum aditibus, propugnaculis insistebant et pinnis, congesta undique saxa telaque habentes in promptu, ut si quis se proripuisset interius, multitudine missilium sterneretur et lapidum.",
	};

	$('#article1 .card-title').html(article1['titre']);
	//$('#article1 .card-img-top').attr('src', article1.image);
	$('#article1 .card-text').html(article1.texte);
	$('#article1 .card-date').html(article1.date);
	$('#article1 .card-auteur').html(article1.auteur);
	$('#article1 .card-lieu').html(article1.lieu);

	// EXO SLIDER

	var slider = [
		'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Sarcelles_-_Hotel_de_ville_-_Panoramique_01.jpg/1280px-Sarcelles_-_Hotel_de_ville_-_Panoramique_01.jpg',
		'http://www.lyc-rousseau-sarcelles.ac-versailles.fr/IMG/jpg/20150401-154-aumis_guylhan-2_copie_2.jpg',
		'http://www.sarcelles.fr/images/5-FAMILLE-ET-SOLIDARITE/5.8-Demarches-logement/D.9_Demarches_logement.jpg'
	];

	// On déclare la variable d'itération
	var i = 1;

	$('#article1 .card-img-top').attr('src', slider[0]);

	$('#article1 .card-img-top').click(function(){
			if (i == slider.length) {
			i = 0;
		}

		$(this).attr('src', slider [i]);
		//$(this).fadeOut();
		i++;
	});

})


//REGARDER LOCAL STORAGE